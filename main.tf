terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Environment = "Test"
      Owner       = "TFProviders"
      Project     = "Test"
    }
  }
}

resource "aws_vpc" "example" {
  cidr_block = "10.1.0.0/16"
  tags = {
    Name = "my-vpc-resource"
  }
}

resource "aws_subnet" "example" {
  cidr_block = "10.1.1.0/24"
  vpc_id     = aws_vpc.example.id
  tags = {
    Name = "my-subnet-resource"
  }
}
